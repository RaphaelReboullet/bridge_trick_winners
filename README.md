# Bridge Trick Winners

Bridge Trick Winners is a program who returns the winners for each trick in a bridge game

## Development environment

### Prerequisite

* PHP 8.1
* Composer
* Symfony CLI

You can check the prerequisites with the following command (from Symfony CLI)

```bash
symfony check:requirements
```

### Launch the development environment

```bash
composer install
symfony serve -d
```

## How to use

1. For the game without trumps : Visit URL `http://localhost:8000/trickWinnersNT?play=YOUR_GAME`.
2. PFor the game with trumps : Visit URL `http://localhost:8000/trickWinners?play=YOUR_GAME&trump=TRUMP`.

Replace `YOUR_GAME` by the cards played (for example `2C-3C-4C-5C-AD-KD-QD-JD-3H`) and `TRUMP` by the trump suit (for example `H` for hearts)

## Launch tests

```bash
php bin/phpunit --testdox
```


