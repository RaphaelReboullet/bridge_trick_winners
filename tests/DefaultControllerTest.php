<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class DefaultControllerTest extends WebTestCase
{
    /**
     * @return void
     */
    public function testTrickWinnersNT(): void
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/trickWinnersNT?play=2C-3C-4C-5C-AD-KD-QD-JD-3H');
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertStringContainsString('["5C","AD"]', $client->getResponse()->getContent());
    }

    /**
     * @return void
     */
    public function testTrickWinners(): void
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/trickWinners?play=2C-3C-4C-5C-AD-KD-QD-JC-3H&trump=C');
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertStringContainsString('["5C","JC"]', $client->getResponse()->getContent());
    }
}
