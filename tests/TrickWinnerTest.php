<?php

namespace App\Tests;

use App\Service\TrickWinner;
use PHPUnit\Framework\TestCase;

class TrickWinnerTest extends TestCase
{
    /**
     * @var TrickWinner
     */
    private TrickWinner $trickWinner;

    /**
     * @return void
     */
    protected function setUp(): void
    {
        $this->trickWinner = new TrickWinner();
    }

    /**
     * @return void
     */
    public function testIsDataValid()
    {
        $validCards = "AH-KH-QH-JH-5C-4C-8D-TC-AH";
        $invalidCards = "AH-KH-QH-10H";
        $invalidTrump = "X";

        $this->assertTrue($this->trickWinner->isDataValid($validCards));
        $this->assertFalse($this->trickWinner->isDataValid($invalidCards));
        $this->assertFalse($this->trickWinner->isDataValid($validCards, $invalidTrump));
    }

    /**
     * @return void
     */
    public function testGetTrickWinnerWithoutTrump()
    {
        $cardsPlayed = "AH-KH-QC-JC-5D";
        $expected = ['AH'];
        $this->assertEquals($expected, $this->trickWinner->getTrickWinner($cardsPlayed));
    }

    /**
     * @return void
     */
    public function testGetTrickWinnerWithTrump()
    {
        $cardsPlayed = "AH-KH-QC-JC-9H";
        $trump = 'C';
        $expected = ['QC'];
        $this->assertEquals($expected, $this->trickWinner->getTrickWinner($cardsPlayed, $trump));
    }
}
