<?php

namespace App\Controller;

use App\Service\TrickWinner;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    /**
     * Method to find the winning cards in case the trump is not defined
     *
     * @param Request $request
     * @param TrickWinner $trickWinner
     * @return JsonResponse
     */
    #[Route('/trickWinnersNT', name: 'trick_winners_nt', methods: ['GET'])]
    public function trickWinnersNT(Request $request, TrickWinner $trickWinner): JsonResponse
    {
        $play = $request->query->get('play');
        if (!$trickWinner->isDataValid($play)) {
            return new JsonResponse([
                'error' => 'Invalid Data'
            ], 400);
        }
        $winner = $trickWinner->getTrickWinner($play);
        return new JsonResponse($winner, 200);
    }

    /**
     * Method to find the winning cards in case the trump is defined
     *
     * @param Request $request
     * @param TrickWinner $trickWinner
     * @return JsonResponse
     */
    #[Route('/trickWinners', name: 'trick_winners', methods: ['GET'])]
    public function trickWinners(Request $request, TrickWinner $trickWinner): JsonResponse
    {
        $play = $request->query->get('play');
        $trump = $request->query->get('trump');
        if (!$trickWinner->isDataValid($play, $trump)) {
            return new JsonResponse([
                'error' => 'Invalid Data'
            ], 400);
        }
        $winner = $trickWinner->getTrickWinner($play, $trump);
        return new JsonResponse($winner, 200);
    }
}
