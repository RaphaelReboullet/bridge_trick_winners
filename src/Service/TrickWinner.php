<?php

namespace App\Service;

class TrickWinner
{
    // Equivalence of card numbers in order to be able to compare their power between them
    private const VALUES = [
        "2" => 2, "3" => 3, "4" => 4, "5" => 5, "6" => 6,
        "7" => 7, "8" => 8, "9" => 9, "T" => 10, "J" => 11,
        "Q" => 12, "K" => 13, "A" => 14
    ];

    // List of existing suits in order to check if the cards given have an existing suit
    private const SUITS = [
        'H', 'D', 'C', 'S'
    ];

    /**
     * Method to explode the cards played in an array in order to manipulate data
     *
     * @param string $cardsPlayed
     * @return array
     */
    private function explodeCardsPlayed(string $cardsPlayed): array
    {
        return explode('-', $cardsPlayed);
    }

    /**
     * Method to check if the data provided is valid
     *
     * @param string $cardsPlayed
     * @param string|null $trump
     * @return bool
     */
    public function isDataValid(string $cardsPlayed, string $trump = null): bool
    {
        // Check if the data is in good format
        if (!preg_match('/^([A-Z0-9]{2}-)+[A-Z0-9]{2}$/', $cardsPlayed)) {
            return false;
        }

        $cardsPlayed = $this->explodeCardsPlayed($cardsPlayed);

        // Check if there is minimum 4 card played
        if (count($cardsPlayed) < 4) {
            return false;
        }

        // Check if there is duplicated cards
        $tricks = $this->splitCardInTricks($cardsPlayed);
        foreach ($tricks as $trick) {
            if (count(array_unique($trick)) != count($trick)) {
                return false;
            }
        }

        // Check if the trump is valid
        if ($trump and !in_array($trump, self::SUITS)) {
            return false;
        }

        // Check if the cards given exists
        foreach ($cardsPlayed as $card) {
            if (strlen($card) != 2) {
                return false;
            }

            $cardSuit = substr($card, -1);
            $cardNumber = substr($card, 0, 1);

            if (!in_array($cardSuit, self::SUITS) or !array_key_exists($cardNumber, self::VALUES)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Method to split the cards in tricks of 4 cards
     *
     * @param array $cardsPlayed
     * @return array
     */
    private function splitCardInTricks(array $cardsPlayed): array
    {
        $tricks = [];
        $countCardsToIgnore = count($cardsPlayed) % 4;
        if ($countCardsToIgnore > 0) {
            array_splice($cardsPlayed, -$countCardsToIgnore);
        }

        for ($i = 0; $i < count($cardsPlayed); $i += 4) {
            $tricks[] = array_slice($cardsPlayed, $i, 4);
        }

        return $tricks;
    }

    /**
     * Method to define the array of cards to use in case there is a trump played
     *
     * @param array $cardsPlayed
     * @param string $trump
     * @return array
     */
    private function getCardsToCompareWithTrump(array $cardsPlayed, string $trump): array
    {
        $cardsPlayedWithTrump = [];
        foreach ($cardsPlayed as $card) {
            $currentCardSuit = substr($card, -1);
            if ($currentCardSuit == $trump) {
                $cardsPlayedWithTrump[] = $card;
            }
        }
        if (!empty($cardsPlayedWithTrump)) {
            $cardsPlayed = $cardsPlayedWithTrump;
        }

        return $cardsPlayed;
    }

    /**
     * Method to return the winning cards
     *
     * @param string $cardsPlayed
     * @param string|null $trump
     * @return array
     */
    public function getTrickWinner(string $cardsPlayed, string $trump = null): array
    {
        $winningCards = [];
        $cardsPlayed = $this->explodeCardsPlayed($cardsPlayed);

        $tricks = $this->splitCardInTricks($cardsPlayed);

        foreach ($tricks as $trick) {
            if ($trump != null) {
                $trick = $this->getCardsToCompareWithTrump($trick, $trump);
            }

            $firstCardSuit = substr($trick[0], -1);
            $firstCardNumber = substr($trick[0], 0, 1);

            $winningCard = $trick[0];
            $winningCardValue = self::VALUES[$firstCardNumber];

            $arrayLenght = count($trick);

            for ($i = 1; $i < $arrayLenght; $i++) {
                $currentCardSuit = substr($trick[$i], -1);
                $currentCardNumber = substr($trick[$i], 0, 1);
                $currentCardValue = self::VALUES[$currentCardNumber];

                if ($currentCardSuit == $firstCardSuit and $currentCardValue > $winningCardValue) {
                    $winningCardValue = $currentCardValue;
                    $winningCard = $trick[$i];
                }
            }
            $winningCards[] = $winningCard;
        }

        return $winningCards;
    }
}
